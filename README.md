# README #

Fellows developers testing

### What is this repository for? ###

* fellows developing testing
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Usefull links ###

* [STEP-BY-STEP GUIDE FOR FRONT-END DEVELOPERS TO GETTING UP AND RUNNING WITH NODE.JS, EXPRESS, JADE, AND MONGODB](http://cwbuecheler.com/web/tutorials/2013/node-express-mongo/)
* [Restful Web App, Node Express and Mongodb](http://cwbuecheler.com/web/tutorials/2014/restful-web-app-node-express-mongodb/)
* [Rest cookbook](http://restcookbook.com/)
* [Shaping up with AngularJS](https://www.codeschool.com/courses/shaping-up-with-angular-js)
* [Build a RESTFul API Using NodeJs and Express 4](https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4)
* [Node Tutorial RESTful APP](https://github.com/cwbuecheler/node-tutorial-2-restful-app)
* [Develop a Restful api Using Node.js With Express](http://pixelhandler.com/posts/develop-a-restful-api-using-nodejs-with-express-and-mongoose)
* [Ferramenta Ungit](https://github.com/FredrikNoren/ungit)

### MONGO RUNNING ###
* SET PATH : "mongod --dbpath PATH/TO/FOLDER_DATA"
* CONNECT : "mongo"
* Switch to Database : "use YOUR_PROJECT"
* Insert example : db.userlist.insert({'username' : 'test1','email' : 'test1@test.com','fullname' : 'Bob Smith','age' : 27,'location' : 'San Francisco','gender' : 'Male'})


### Others Stuff ###
* [CSS Gradiant Generator CSS/](http://www.gradient-animator.com/)
* [Mobile AppGyver](http://www.appgyver.com/supersonic)
* [Ionic: Advanced HTML5 Hybrid Mobile App Framework](http://ionicframework.com/)

### Who do I talk to? ###

* Ricardo Leite