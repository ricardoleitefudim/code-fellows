var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    db.collection('userlist').find().toArray(function (err, items) {
        res.json(items);
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    db.collection('userlist').insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

/*
 * UPDATE to updateuser.
 */
router.put('/edituser/:id', function(req, res)
{
	var db = req.db;
    var userToEdit = req.params.id;

	var ObjectID = require('mongodb').ObjectID;
	var o_id = new ObjectID(userToEdit);

	db.collection('userlist').update({_id: o_id},{$set: req.body},function(err,result)
	{
		res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
	});

	//http://docs.mongodb.org/manual/reference/object-id/

});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var userToDelete = req.params.id;
    db.collection('userlist').removeById(userToDelete, function(err, result) {
        res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
    });
});


module.exports = router;